import { Component } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';
import { SessionService } from './services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pokedex';

  constructor(private auth: AuthGuard, private session: SessionService, private router: Router){}

  get isAuth(){
    return [AuthGuard] && this.session.getTrainerSession();
  }
  get username(){
    return this.isAuth ? (this.session.getTrainerSession()) : null;
  }

  logOut(){
     //Bit misleading, but while in dev, logout might as well clear.
    this.session.clearAllStorage();
    console.log("Clearing all")
    this.router.navigateByUrl('/home');
  }
}
