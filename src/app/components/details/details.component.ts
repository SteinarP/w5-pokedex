import { Component, OnInit, Input } from '@angular/core';
import { PokeapiService } from 'src/app/services/pokeapi.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  private pokemonID: number; //Requested details ID.
  private result: any;
  public pokemon : Pokemon;
  public sprite: string;
  public got: number;
  //@Input() preview: Pokemon;

//SessionService
  constructor(private pokeapi: PokeapiService, private session: SessionService, private route: ActivatedRoute) { }

  async ngOnInit() {
    console.log("Initing catalogue");
    try {
      this.pokemonID = Number(this.route.snapshot.params.id);
      this.result = await this.pokeapi.getPokemonById(this.pokemonID);
      this.pokemon = this.result;
      console.log("Returned pokemon with name:", this.pokemon.name)

      this.sprite = this.pokeapi.getImageById(this.pokemon.id, 'artwork');
    } /*gotto*/ catch (em) /*all*/ {
      console.error(em.message);
    }
  }

  onCatchClicked(){
    console.log(`You caught a ${this.pokemon.name}!`);
    //let pokemonPreview = new pokemon(); {name: this.pokemon.name, id: this.pokemonID};
    let pokemonPreview = {name: this.pokemon.name, id: this.pokemonID};
    this.session.addPokemonToCollection(pokemonPreview);
    this.got++;
  }
}
