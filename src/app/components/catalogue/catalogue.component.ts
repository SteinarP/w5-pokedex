import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { PokeapiService } from 'src/app/services/pokeapi.service'
import { Pokemon } from 'src/app/models/pokemon.model';



@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

  //pokemon: string[] = ["Alpha", "Bravo", "Charlie"];
  pokeinfo : any;
  pokemonArray: Pokemon[];

  constructor(private router: Router, private session: SessionService, private pokeapi: PokeapiService) { }

  async ngOnInit() {
    console.log("Initing catalogue");
    try {
      this.pokeinfo = await this.pokeapi.getPokemonRange();
      this.pokemonArray = this.pokeinfo.results;
      console.log("Loaded 100 pokemon:", this.pokemonArray.map(e => e.name+","), "-that's all.");
    } /*gotto*/ catch (em) /*all*/ {
      console.error(em.message);
    }
  }

  onCardClicked(id: number){
    //console.log("Going to details page for pokemon with id", id);
    this.router.navigateByUrl(`/pokemon/${id}`)
  }
}
