import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pokemon } from '../../models/pokemon.model';
import { PokeapiService } from 'src/app/services/pokeapi.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-pokecard',
  templateUrl: './pokecard.component.html',
  styleUrls: ['./pokecard.component.css']
})
export class PokecardComponent implements OnInit {

  @Input() preview: Pokemon;
  @Output() cardClicked: EventEmitter<number> = new EventEmitter();
  /*  @Output() completed: EventEmitter<number> = new EventEmitter();
    onItemClicked(): void{
    this.completed.emit(this.todoItem.id);
  }*/
  public sprite: string;

  constructor(private pokeapi: PokeapiService, private router: Router, private session: SessionService) { }

  ngOnInit(): void {
    //const base = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/";
    //console.log("Rendering card of:", this.preview)
    console.log("Using Pokemon preview data: ", this.preview);

    if (this.preview.hasOwnProperty('id') === false){
      if (this.preview.hasOwnProperty('url')){
        this.preview.id = this.pokeapi.getIdByUrl(this.preview.url);
        console.log("Set ID from URL:", this.id);
      } else {
        console.error("Pokemon preview object has neither ID nor URL!");
      }
    }

    if (this.preview.id){
      this.sprite = this.pokeapi.getImageById(this.preview.id);
    } else {
      this.sprite = 'https://image.flaticon.com/icons/svg/188/188918.svg';
    }
    //console.log("Inited pokecard with id", this.id)


  }

  get isCaught(){
    let collection = this.session.getFullPokemonCollection();
    if (!collection || collection.length === 0) return false;
    collection = this.session.getFullPokemonCollection().map(e=>e.id)
    console.log("ID:", this.preview.id, "Got collection:", collection)
    return collection.includes(this.preview.id);
  }

  get id(){
    return this.preview.id;
  }

  onCardClicked(): void{
    this.cardClicked.emit(this.id);
    console.log("Redirecting to details page with id", this.id);
    this.router.navigate(['/pokemon', this.id]);
  }
}
