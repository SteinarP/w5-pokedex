import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {
  public trainername: string;
  public pokemonCollection: Pokemon[];

  constructor(private router: Router, private session: SessionService) { }

  ngOnInit(): void {
    this.trainername = this.session.getTrainerSession();
    this.pokemonCollection = this.session.getFullPokemonCollection();
    console.log("Loaded collection for", this.trainername,"\n", this.pokemonCollection);
  }

}
