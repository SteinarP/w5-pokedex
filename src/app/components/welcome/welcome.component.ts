import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { SessionService } from '../../services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
  })

  isLoading: boolean = false;

  constructor(private session: SessionService, private router: Router) { }

  ngOnInit(): void {
  }

  get username(){
    return this.loginForm.get('username');
  }

  async onLoginClicked(){
    this.session.newTrainerSession(this.username.value);
    console.log("Saved, get'd username: ", this.session.getTrainerSession());

    this.router.navigateByUrl('/catalogue');
  }

}
