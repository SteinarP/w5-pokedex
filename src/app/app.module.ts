import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { DetailsComponent } from './components/details/details.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { PokecardComponent } from './components/pokecard/pokecard.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    TrainerComponent,
    CatalogueComponent,
    DetailsComponent,
    PagenotfoundComponent,
    PokecardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
