import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { DetailsComponent } from './components/details/details.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: "home",
    component: WelcomeComponent,
  },
  {
    path: "",
    pathMatch: 'full',
    redirectTo: '/home',
  },
  {
    path: "catalogue",
    component: CatalogueComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: "pokemon/:id",
    component: DetailsComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: "trainer",
    component: TrainerComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: "404",
    component: PagenotfoundComponent,
  },
  {
    path: "**", 
    redirectTo: '/404',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
