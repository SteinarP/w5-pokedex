## Pokemon TODO
# Setup:
- Init Angular
- Make empty pages
- Route to all

# Landing page
  - Input trainer name, storing locally.
  - If name saved, redirect to Pokemon page.

# Trainer page
  - Display list of all Pokemon trainer has collected (avatars)
  - All pokemon can be clicked to go to their detail page.

# Pokemon catalogue page
  - Shows all Pokemon
  - Inaccessible unless they've got a trainer name
  - "Card style" list of Pokemon with image and name
  - All pokemon can be clicked to go to their detail page.

# Pokemon detail page
  - Base stats
    - Name
    - Image
    - Types
    - Base stats
  - Profile
    - Height
    - Weight
    - Abilities
    - Base Experience
  - Moves
    - List of this one's moves.
  - Button adding pokemon to trainer's Collection