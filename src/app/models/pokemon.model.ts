export interface Pokemon {
    abilities: any[];
    base_experience: number;
    forms: any[];
    game_indices: any[];
    height: number;
    held_items: any[];
    id: number;
    is_default: boolean;
    location_area_encounters: LinkStyle;
    moves: any[];
    name: string;
    order: number;
    species: object;
    sprites: object;
    stats: any[];
    types: any[];
    weight: number;
    url: string;
}
