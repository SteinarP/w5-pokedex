import { Injectable } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }
  set(key: string, item: any): void{
    localStorage.setItem(key, JSON.stringify(item));
  }

  get(key): any{
    const item = localStorage.getItem(key);
    return item ? JSON.parse(item) : false;
  }

  newTrainerSession(name: string){
    this.set("trainer", name);
  }

  getTrainerSession(): any{
    return this.get("trainer");
  }

  addPokemonToCollection(pokemon: any){
    const current = this.get('collection');
    if (!current ){
        this.set('collection', [pokemon]);
        return;
    }
    let colEdit = current.slice(); //clone
    colEdit.push(pokemon);
    this.set("collection", colEdit);
  }

  getFullPokemonCollection(){
    return this.get("collection")
  }

  removePokemonFromCollection(name:string, id=0){
    //TODO
  }

  removeAllPokemon(){
    let len = this.get('collection').length;
    this.set('collection', []);
    console.log(`Set free ${len} Pokémon.`);
  }

  clearAllStorage(){
    localStorage.clear();
    sessionStorage.clear();
    console.log("All clear.");
  }
}


