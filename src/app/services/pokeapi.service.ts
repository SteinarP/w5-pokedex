import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokeapiService {

  constructor(private http: HttpClient) { 
  }

  getBulbasaurTest(){
    const url = "https://pokeapi.co/api/v2/pokemon/1/";
    return this.http.get(`${url}`).toPromise();
  }  

  getPokemonById(id: number){
    const url = env.apiUrl+`/pokemon/${id}`;
    return this.http.get(`${url}`).toPromise();
  }  

  getPokemon(url: string){
    return this.http.get(`${url}`).toPromise();
  }  

  getPokemonRange(limit:number=100, offset:number=0){
    const url = env.apiUrl+`/pokemon/?limit=${limit}&offset=${offset}`;
    return this.http.get(`${url}`).toPromise();
  }


  getIdByUrl(url: string){
    //"https://pokeapi.co/api/v2/pokemon/1/"
    //filter(Boolean) removes falsey items like empty strings. So we pop the last non-empty.
    let idStr = url.split("/").filter(Boolean).pop();
    return parseInt(idStr);
  }

  getImageByUrl(url: string){
    return this.getImageById(this.getIdByUrl(url));
  }

  getImageById(id: number, version="default"){
    let img = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/";
    switch (version){
      case 'artwork': img += "/other/official-artwork"; break;
      case 'back': img +="/back"; break;
      case 'shiny': img+="/shiny"; break;
      default: break;
    }
    return `${img}/${id}.png`;
  }
}
